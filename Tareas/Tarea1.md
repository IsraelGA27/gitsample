# **ESPÍRITU BÚHO**

_Te contaron que __desaparecí__,
pero sabes que no es verdad._
_Adopté la forma de la ausencia quedándome en el __umbral__ de tu casa._
_De vez en cuando riego tus plantas, tu perro me ladra y tu vecino __fallecido__ hace como que acaricia a su esposa._
_Duermes tranquila en tu refugio rodeada de una __oscura laguna__, porque sabes que mi **espíritu búho** te cuida desde la ventana._

> Autor: **Anonimo.**
